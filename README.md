Отчет о проделанной работе.
---------

1). Реализована витрина online-магазина с категориями.
    1.1Создана модель StoreItem содержащая характеристики объекта(товара);
    редактирование, правка, удаление и т.п реализуется при использовании прав администратора django;
    Главная страница - есть Queryset из 4 товаров отсортированных по значению "price";
    (шаблону post_list передается список записей массива arr из 4х эл-ов);
    аналогично для основной витрины;
    реализована страница каждого товара (посредством получения id-товара, аналогично для категорий (хранится по умолчанию в модели);
    создано разделение на категории (модель, связанная со StoreItem)
    
2). Корзина пользователя, в которую можно добавлять или удалять товары.
    Созданы 2 модели: StoreCart (содержит только id корзины, для разграничения между пользователями) и CartItem(является связкой между товарами магазина и корзинами пользователей);
    Сохранение корзины в Cookies (ф-ии set_cart_id и get_cart_id для задания корзины пользователю и извлечение id для последующего функционирования системы); 
    возможность добавить и удалить товар из корзины (функции cart_add и cart_delete Получение id корзины и предмета и сохранение его в конкретной корзине, получение id предмета находящегося внутри конкретной корзины и последующее его удаление);
    Страница корзины (отображение всех товаров в корзине и total суммы данных товаров);
    
3). После оформления заказа, письмо с данными о заказе и пользователе отправляется на почту владельца магазина
    (функция order) отправление письма владельцу магазина и пользователю, сделавшему заказ, с указанием купленных товаров и итоговой суммы (from django.core.mail import send_mail, указание в local.settings значений EMAIL_HOST, EMAIL_HOST_USER и т.д.);  
    обработка ошибки неправильного ввода e-mail;
    
4). Приложение работает с неэлектронными товарами, меняется количество доступного товара
    (функция order) Количество товара изменяется при оформлении заказа (при отсутсвиии товара "на складе", его нельзя положить в корзину)
    Рассмотрен случай возможного получения отрицательного значения количества товаров (заказ будет оформлен только на те товары, что есть в наличии, после чего значение qty станет равным 0)
    Также возможно изменить количество товара воспользовавшись правами администратора в Django.
    
 