from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^store', views.store_main, name='store_main'),
    url(r'^category/(?P<categ_id>\d*)', views.categ, name='categ'),
    url(r'^item/(?P<item_id>\d*)/add', views.cart_add, name='cart_add'),
    url(r'^cart/delete/(?P<cart_item_id>\d*)', views.cart_delete, name='cart_delete'),
    url(r'^item/(?P<item_id>\d*)', views.item_description, name='item_description'),
    url(r'^cart', views.cart_page, name='cart_page'),
    url(r'^payment/cart/$', views.paypal_pay, name='cart'),
    #url(r'^payment/success/$', views.paypal_success, name='success'),
    url(r'^order', views.order, name='order'), 
]
