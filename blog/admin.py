from django.contrib import admin
from .models import Post, StoreItem, CartItem, StoreCart, Categ

admin.site.register(Post)
admin.site.register(StoreItem)
admin.site.register(CartItem)
admin.site.register(StoreCart)
admin.site.register(Categ)