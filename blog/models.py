from django.db import models
from django.utils import timezone


class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class StoreItem(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    price = models.IntegerField()
    qty = models.IntegerField()
    category = models.CharField(max_length=200, default = "String")
    img = models.TextField()
    def price_rub(self):
        return (self.price/100)

    def __str__(self):
        return self.name
class StoreCart(models.Model):
    def __str__(self):
        return str(self.id)
class CartItem(models.Model):
    item = models.ForeignKey(StoreItem)
    cart = models.ForeignKey(StoreCart)
    def __str__(self):
        return str(self.id)

class Categ(models.Model):
    name = models.CharField(max_length=100)
    stuff = models.ManyToManyField(StoreItem)
    
    def __str__(self):
        return self.name