# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_storeitem_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categ',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('stuff', models.ManyToManyField(to='blog.StoreItem')),
            ],
        ),
    ]
