# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20160318_2307'),
    ]

    operations = [
        migrations.AddField(
            model_name='storeitem',
            name='category',
            field=models.CharField(max_length=200, default='String'),
        ),
    ]
