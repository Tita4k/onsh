from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from .models import Post, StoreItem, CartItem, StoreCart, Categ
from django.core.mail import send_mail
import re
from paypal.standard.forms import PayPalPaymentsForm
#from paypal.standard.models import ST_PP_COMPLETED
#from paypal.standard.ipn.signals import valid_ipn_received


def cart_add(request, item_id = None):
    cart_id = get_cart_id(request)
    a = CartItem()
    item = StoreItem.objects.get(id = item_id)
    if (item.qty == 0):
        return set_cart_id(render(request, 'blog/item_description_nomore.html', {'item': item, 'cart_id': cart_id}), cart_id)    
    cart = StoreCart.objects.get(id = cart_id)
    #item.qty = item.qty - 1
    #item.save()
    #if (item.qty == -1):
     #   item.qty = item.qty + 1
      #  item.save() 
    #else:
    a.item = item
    a.cart = cart
    a.save() 
    return set_cart_id( HttpResponseRedirect("/cart/"+str(cart_id)), cart_id)
    
def cart_delete(request, cart_item_id = None):
    a = CartItem.objects.get(id = cart_item_id)
    cart_id = a.cart.id
    #a.item.qty = a.item.qty + 1
    #a.item.save()
    a.delete()
    return set_cart_id(HttpResponseRedirect("/cart/"+str(cart_id)), cart_id)
    
def post_list(request):
    posts = StoreItem.objects.order_by("-price")
    arr = []
    for i in range (0, 4):
        arr.append(posts[i])
    #cart_id = get_cart_id(request)
    return set_cart_id(render(request, 'blog/post_list.html', {'arr': arr}), get_cart_id(request))
def get_cart_id(request):
    if 'cart_id' in request.COOKIES:
        return request.COOKIES[ 'cart_id' ]
    else:
        cart = StoreCart()
        cart.save()
        return cart.id
def set_cart_id(response, cart_id):
    response.set_cookie( 'cart_id', str(cart_id) )
    return response
def store_main(request):
    title = Categ.objects.all()
    items = StoreItem.objects.order_by("-price")
    #arr = []
    #for i in range(len(items)):
     #   if (i%5==0):
      #      arr.append([])
       # arr[-1].append(items[i])
    
    return set_cart_id(render(request, 'blog/store_main.html', {'items': items, 'title': title}), get_cart_id(request))

def categ(request, categ_id = None):
    title = Categ.objects.all()
    categ = Categ.objects.get(id = categ_id)
    items = categ.stuff.all()
    #arr = []
    #for i in range(len(items)):
     #   if (i%5==0):
      #      arr.append([])
       # arr[-1].append(items[i])
    return set_cart_id(render(request, 'blog/store_category.html', {'items': items, 'title': title}), get_cart_id(request))

def item_description(request, item_id = None):
    item = StoreItem.objects.get(id = item_id)
    cart_id = get_cart_id(request)
    if (item.qty == 0):
        return set_cart_id(render(request, 'blog/item_description_nomore.html', {'item': item, 'cart_id': cart_id}), cart_id)    
    return set_cart_id(render(request, 'blog/item_description.html', {'item': item, 'cart_id': cart_id}), cart_id)
    
def cart_page(request):
    cart_id = get_cart_id(request)
    items = CartItem.objects.filter(cart = cart_id)
    total = 0
    for item in items:
        total = total + item.item.price
    total = total/100
    return set_cart_id(render(request, 'blog/cart_page.html', {'items': items, 'total': total}), cart_id)
 
def order(request, cart_item_id = None):
    cart_id = get_cart_id(request)
    items = CartItem.objects.filter(cart = cart_id)
    total = 0
    for item in items:
       total = total + item.item.price
    total = total/100
    items = CartItem.objects.filter(cart = cart_id)
    items = [item.item.name for item in items]
    ##total = 777 # TODO: calculate total price
    email = request.GET.get('email', '')
    tmail = re.findall(r'@\w+', email)
    if (len(tmail) == 0):
        items = CartItem.objects.filter(cart = cart_id)
        for item in items:
            item.item.qty = item.item.qty+1
            item.item.save()
        return set_cart_id(render(request, 'blog/cart_page_error.html', {'items': items, 'total': total}), cart_id)        
    msg1 = 'contact us: kirill.forever.10@gmail.com\nprice: ' + str(total) + ' RUB\n' + str(items)
    msg = 'customer: ' + email + '\nprice: ' + str(total) + ' RUB\n' + str(items)
    send_mail('Thank you', msg1, 'Kek.INC', [str(email)], fail_silently=False)
    send_mail('Order', msg, 'Kek.INC', ['kirill.forever.10@gmail.com'], fail_silently=False)
    new_cart = StoreCart()
    new_cart.save()
    kek = StoreItem.objects.order_by("price")
    return set_cart_id(render(request, 'blog/order.html', {'items': items, 'kek': kek, 'cart_id': cart_id}), new_cart.id)

# Create your views here.

def paypal_pay(request, cart_item_id = None):
    cart_id = get_cart_id(request)
    items = CartItem.objects.filter(cart = cart_id)
    kek = ''
    for item in items:
        kek += item.item.name
        kek += '; '
    total = 0
    for item in items:
        if (item.item.qty - 1 < 0):
            item.item.qty = 0
            item.item.save()
            item.delete()
        else:
            item.item.qty = item.item.qty-1
            item.item.save()
            total = total + item.item.price
    total = total/100
    # What you want the button to do.
    paypal_dict = {
        "business": "tita4k@yandex.ru",
        "amount": str(total),
        "item_name": kek,
        "invoice": "unique-invoice-id",
        "notify_url": "https://www.example.com",# + reverse('paypal-ipn'),
        "return_url": "https://tita4k.pythonanywhere.com/order",
        "cancel_return": "http://tita4k.pythonanywhere.com/cart",
        "currency_code": "RUB",
        "custom": "Upgrade all users!",  # Custom command to correlate to some function later (optional)
    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form}
    return render(request, 'blog/payment.html', context)

    #def paypal_success(request):
       # ipn_obj = sender
    #if ipn_obj.payment_status == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the business field request. (The user could tamper
        # with those fields on payment form before send it to PayPal)
        #if ipn_obj.receiver_email != "receiver_email@example.com":
            # Not a valid payment
           # return

        # ALSO: for the same reason, you need to check the amount
        # received etc. are all what you expect.

        # Undertake some action depending upon `ipn_obj`.
       #3 if ipn_obj.custom == "Upgrade all users!":
        #    Users.objects.update(paid=True)
   # else:
        #...

#valid_ipn_received.connect(show_me_the_money)
    