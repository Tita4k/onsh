from django.conf.urls import include, url
from django.contrib import admin
from django.http import HttpResponse

def test(request):
    return HttpResponse("kek")

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    url(r'', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^test/', test),
]
